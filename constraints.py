from pypoman.polyhedron import compute_chebyshev_center
from cvxopt import matrix, solvers
from mathfun import vecnorm
import numpy as np
import logging

class Inequality:
    def __init__(self, A ,b):
        self.A = A
        self.b = b

    def checkEquality(self, x):
        epsilon = self.A @ x - self.b
        logging.debug('checking active constraints, Ax-b: {}'.format(epsilon))
        active_rows = np.where(epsilon.all(axis=1)==False)
        return active_rows[0]

    def plotRegion(self):
        return 0


class Region(Inequality):
    def __init__(self, A, b):
        super(Region, self).__init__(A, b)


    def getChebyshev(self):
        x0 = compute_chebyshev_center(self.A, self.b)
        if x0.all():
            return x0[:, np.newaxis]
        else:
            x = x0 + 0.001*np.ones(x0.shape)
            return x[:, np.newaxis]


    def getInteriorPtConstr(self, G, S, W):
        T_tmod = np.hstack((vecnorm(self.A, 'r'), self.A))
        A_upper  = np.hstack((T_tmod, np.zeros((self.A.shape[0], G.shape[1]))))
        A_lower = np.hstack((np.zeros((S.shape[0], 1)), np.hstack((-S, G))))
        A_constr = np.vstack((A_upper, A_lower))
        logging.debug('A matrix for Interior point is {}'.format(A_constr))
        B_constr = np.vstack((self.b, W))
        logging.debug('B matrix for Interior point is {}'.format(B_constr))
        c_constr = np.vstack((-np.ones((1,1)), np.vstack((np.zeros((self.A.shape[1],1)), np.zeros((G.shape[1],1))))))
        logging.debug('C matrix for Interior point is {}'.format(c_constr))
        sol = solvers.lp(matrix(c_constr,tc='d'), matrix(A_constr, tc='d'), matrix(B_constr, tc='d'))
        optimum = sol['x']
        logging.info('LP solved for Interior Point in Region.. \n Radius is {} \n optimum is {}'.format(optimum[0], optimum[1:]))
        return optimum[0], np.array(optimum[1:self.A.shape[1]+1])

