import numpy as np
from numpy.linalg import inv, matrix_rank
from constraints import Region
from cvxopt import matrix, solvers
import logging

logging.basicConfig(level=logging.DEBUG)


A = np.array([[0.7326, -0.0861],[0.1722, 0.9909]])
B = np.array([[0.0609],[0.0064]])

u_max = 2
u_min = -2


H = np.array([[1.5064, 0.4838],[0.4838, 1.5258]])

F = np.array([[9.6652, 5.2115], [7.0732, -7.087]])

G = np.array([[1, 0],[-1, 0],[0, 1],[0, -1]])

W = 2*np.ones((4,1))

E = np.zeros((4,2))

S = E + G @ inv(H) @ F.T

# some constants
x1_min = -1.5; x1_max = 1.5
x2_min = -1.5; x2_max = 1.5
Ax  = np.array([[1, 0],[-1, 0],[0, 1],[0, -1]]) ; bx =  x1_max*np.ones((4,1))

# algorithm starts here
cr_all = []


region_curr = Region(Ax, bx)


# step 1
def partition(rgn):
    CR_unexplored = [rgn]
    CR_explored = []
    CR_feasible = []
    CR_infeasible = []
    epsilon, x0 = rgn.getInteriorPtConstr(G=G, S=S, W=W)
    logging.debug('x0 is {}'.format(x0))
    if epsilon > 0:
        P = matrix(H, tc='d'); q = matrix(np.zeros(x0.shape), tc='d')
        Gqp = matrix(G, tc='d'); h = matrix(W) + matrix(S @ x0)

        qp_sol = solvers.qp(P=P, q=q, G=Gqp, h=h)
        z0 = qp_sol['x']
        active_ind = rgn.checkEquality(z0)
        if not active_ind.size==0:
            G_t = A[active_ind,:]
            W_t = W[active_ind,:]
            S_t = S[active_ind,:]
            K_CR = G@inv(H)@G_t.T@inv(G_t@inv(H)@G_t.T)
            CR0 = Region(K_CR@S_t - S, W-K_CR@W_t)
        else:
            CR0 = Region(-S, W)
        CR_all = []
        m = rgn.b.shape[0]
        for i in range(m):
            identity_ij = np.eye(m)
            identity_ij[i, i] = -1
            A_ij = identity_ij @ CR0.A
            b_ij = identity_ij @ CR0.b
            logging.info('test exploration,\n Ax:{},\n Aij: {}, bij: {}'.format(Ax, A_ij[0:i + 1, :], b_ij[0:i + 1, :]))
            region_ij = Region(A_ij[0:i + 1, :], b_ij[0:i + 1, :])
            CR_all.append(CR_ij)
            qp_sol_ij, CR_ij = partition(region_ij)
            logging.info('CR_ij is CR.A: {}, CR.b: {}'.format(CR_ij.A, CR_ij.b))

    return qp_sol, CR



if __name__ == '__main__':
    #calulcate CR0
    region_start = Region(Ax, bx)
    qp_sol, CR0 = partition(region_start)
    logging.info('solution to qp obtained, optimizer:{}\
                  best value:{}, CR0.A:{}, CR0.b:{}'.format(qp_sol['x'], qp_sol['primal objective'], CR0.A, CR0.b))

    CR_all = []
    #explore the rest of the region



