import numpy as np
import matplotlib.pyplot as plt


if __name__ == '__main__':
    u = np.linspace(1,10,10)
    v = np.linspace(1,10,10)
    x, y = np.meshgrid(u,v)
    cond = x + y < 3 # and (y + 2*x  < 3)
    print(x.shape)
