from numpy.linalg import inv
from constraints import Region
import numpy as np

H = np.array([[1.5064, 0.4838],[0.4838, 1.5258]])

F = np.array([[9.6652, 5.2115], [7.0732, -7.087]])

G = np.array([[1, 0],[-1, 0],[0, 1],[0, -1]])

W = 2*np.ones((4,1))

E = np.zeros((4,2))

S = E + G @ inv(H) @ F.T

# some constants
x1_min = -1.5; x1_max = 1.5
x2_min = -1.5; x2_max = 1.5
Ax  = np.array([[1, 0],[-1, 0],[0, 1],[0, -1]]) ; bx =  x1_max*np.ones((4,1))


region_curr = Region(Ax, bx)
epsilon, interiorPt = region_curr.getInteriorPtConstr(G,S,W)
print(epsilon, interiorPt)
