import numpy as np
from numpy.linalg import norm


def vecnorm(m, axis):
    if not isinstance(m, np.ndarray):
        raise TypeError('argument must be of type \'numpy.ndarray\'')
    else:
        nrows = m.shape[0]
        ncolumns = m.shape[1]

        if axis=='r':
            out = np.zeros((nrows,1))
            for i in range(nrows):
                rnorm = norm(m[i,:])
                out[i,0] = rnorm
        elif axis =='c':
            out = np.zeros((1,ncolumns))
            for i in range(ncolumns):
                rnorm = norm(m[:,i])
                out[0,i] = rnorm
    return out

if __name__ == '__main__':
    a = np.array([[1,2],[3,4]])
    b = vecnorm(a,'c')
